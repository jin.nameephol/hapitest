'use strict';

const Hapi = require('@hapi/hapi');
const users = require('./users');

const init = async () => {

    const server = Hapi.server({
        port: 3000,
        host: 'localhost',
        routes: {
            cors: true
        }
    });

    // //home route
    server.route({
        method: 'GET',
        path: '/',
        handler: (request, h) => {
            request.logger.info('In handler %s', request.path)

            return ('<h1>Hello World!</h1>');
        }
    });

    server.route({
        method: 'GET',
        path: '/users',
        handler: function (request, h) {
            return (users.findAll());
        }
    });

    server.route({
        method: 'GET',
        path: '/users/{id}',
        handler: function (request, h) {
            var id = request.params.id;
            return (users.findById(id));
        }
    });

    server.route({
        method: 'POST',
        path: '/newuser',
        handler: function(request, reply) {
          user = {
            id: request.payload.id,
            username: request.payload.fistname,
            name: request.payload.lastname,
            position: request.payload.address
          }
          users.save(user);
          reply(user);
        }
      });
      

    //Dynamic Route
    server.route({
        method: 'GET',
        path: '/hello/{user}',
        handler: function (request, h) {

            return `Hello ${encodeURIComponent(request.params.user)}!`;
        }
    });

    await server.register(require('inert'));

    server.route({
        method: 'GET',
        path: '/about',
        handler: (request, h) => {
            return h.file('./public/about.html')
        }
    });

    await server.register({
        plugin: require('hapi-pino'),
        options: {
            prettyPrint: false,
            logEvent: ['response']
        }
    });

    await server.start();
    console.log('Server running on %s', server.info.uri);
};
process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});


init();