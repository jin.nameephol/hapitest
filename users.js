var users = [
    {
        "id": 1,
        "firstname": "goldroger",
        "lastname": "Gol D. Roger",
        "address": "Pirate King"
    }
];

exports.findAll = function() {
    return users;
};

exports.findById = function (id) {
    for (var i = 0; i < users.length; i++) {
        if (users[i].id == id) return users[i];
    }
};

exports.save = function(user) {
    users.push(user);
}